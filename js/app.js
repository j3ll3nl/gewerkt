$(document).foundation();

// Extend the default picker options for all instances.
$.extend($.fn.pickatime.defaults, {
    format: 'H:i',
    interval: 15,
    min: [7,0],
    max: [21,30]
});

break_$input = $('.break-time').pickatime({
    clear:'',
    min: [0,0],
    max: [2,0]

});

break_picker = break_$input.pickatime('picker');

break_picker.set('select', '0:30', { format: 'H:i' });

/**
 * scripts
 */
var start_$input = $('.start-time').pickatime(),
    start_picker = start_$input.pickatime('picker');

var end_$input = $('.end-time').pickatime({
        formatLabel: function( timeObject ) {
            var breakObject = break_picker.get('select'),
                minObject = this.get( 'min' ),
                hours = timeObject.hour - minObject.hour - breakObject.hour,
                mins = ( timeObject.mins - minObject.mins - breakObject.mins ) / 60;
            return '<b>H</b>:i <sm!all>(' + (hours + mins) + ' !uur)</sm!all>';
        }
    });
    end_picker = end_$input.pickatime('picker');


// Check if there’s a “from” or “to” time to start with.
if ( start_picker.get('value') ) {
    end_picker.set('min', start_picker.get('select'))
}
if ( end_picker.get('value') ) {
    start_picker.set('max', end_picker.get('select') )
}

// When something is selected, update the “from” and “to” limits.
start_picker.on('set', function(event) {
    if ( event.select ) {
        end_picker.set('min', start_picker.get('select'))
    }
});
end_picker.on('set', function(event) {
    if ( event.select ) {
        start_picker.set('max', end_picker.get('select'))
    }
});

// Define the `phonecatApp` module
var workingHoursApp = angular.module('workingHoursApp', []);

// Define the `PhoneListController` controller on the `phonecatApp` module
workingHoursApp.controller('workingHoursCtlr', function workingHoursCtlr($scope) {
    var defaultDate = new Date("01-01-2016 00:00");

    $scope.defaultDays = function() {
        $scope.days = [
            {
                name: 'Ma',
                time: defaultDate
            }, {
                name: 'Di',
                time: defaultDate
            }, {
                name: 'Wo',
                time: defaultDate
            }, {
                name: 'Do',
                time: defaultDate
            }, {
                name: 'Vr',
                time: defaultDate
            }, {
                name: 'Za',
                time: defaultDate
            }, {
                name: 'Zo',
                time: defaultDate
            }
        ];

        $scope.totalWeek = 0;

    };

    $scope.setWeekTotal = function() {
        var totalWeek = 0;

        $scope.days.forEach( function(day) {
            var worked =+ day.time.getHours() + (day.time.getMinutes() / 60);
            totalWeek += parseFloat(worked);
        });

        $scope.totalWeek = totalWeek;
    };

    $scope.defaultDays();

    $scope.dateProcent = function(date) {
        return date.getHours() + (date.getMinutes() / 60);
    };

    $scope.updateDay = function(day) {
        var startMinutes = start_picker.get('select').time;
        var breakMinutes = break_picker.get('select').time;
        var endMinutes = end_picker.get('select').time;
        var minutes = (endMinutes-breakMinutes-startMinutes)*60000;

        day.time = new Date(defaultDate.getTime()+minutes);

        $scope.setWeekTotal();
    }
});